package com.test.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.test.Model.User;
import com.test.Repository.UserRepository;

@Service
public class UserService { // business logic 
	@Autowired
	private UserRepository userRepository;
	public User CreateUser(User user)
	{
		return userRepository.save(user);
		
	}

}
